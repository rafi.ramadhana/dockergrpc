# dockergrpc

## Why?

`google.golang.org/protobuf` does not support plugins anymore, while `github.com/golang/protobuf` still supports plugins.

This project enables you to switch between `google.golang.org/protobuf` and `github.com/golang/protobuf` conveniently.

https://stackoverflow.com/a/61054866

## How to use

To use `github.com/golang/protobuf`

1. Setup the `.env`, match the `PROTO_SOURCE_PATH` with your local
2. Run `make build-github`
3. Run `make run-github`
4. Proto files will be generated in the `out` folder

To use `google.golang.org/protobuf`

1. Setup the `.env`, match the `PROTO_SOURCE_PATH` with your local
2. Run `make build-google`
3. Run `make run-google`
4. Proto files will be generated in the `out` folder

P. S. If your project still has 2 separated protos for _models_ and _services_, perhaps you are using the `github.com/golang/protobuf`
