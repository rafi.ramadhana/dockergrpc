build-google:
	export $$(cat .env | xargs) && \
	docker build \
		-f GoogleDockerfile \
		--build-arg PROTOC_VERSION=$$PROTOC_VERSION \
		--build-arg PROTOC_GEN_GO_VERSION=$$PROTOC_GEN_GO_VERSION \
		--build-arg PROTOC_GEN_GO_GRPC_VERSION=$$PROTOC_GEN_GO_GRPC_VERSION \
		--platform linux/x86_64 \
		-t dockergrpc:$$DOCKERGRPC_GOOGLE_IMAGE_TAG \
		.

build-github:
	export $$(cat .env | xargs) && \
	docker build \
		-f GithubDockerfile \
		--build-arg PROTOC_VERSION=$$PROTOC_VERSION \
		--build-arg GITHUB_PROTOC_GEN_GO_VERSION=$$GITHUB_PROTOC_GEN_GO_VERSION \
		--platform linux/x86_64 \
		-t dockergrpc:$$DOCKERGRPC_GITHUB_IMAGE_TAG \
		.

run-google:
	make clear && \
	export $$(cat .env | xargs) && \
	docker run \
		--platform linux/x86_64 \
		-v $$(pwd):/app \
		-v $$PROTO_SOURCE_PATH:/app/in \
		-it \
		--rm \
		dockergrpc:$$DOCKERGRPC_GOOGLE_IMAGE_TAG \
		make generate-google

run-github:
	make clear && \
	export $$(cat .env | xargs) && \
	docker run \
		--platform linux/x86_64 \
		-v $$(pwd):/app \
		-v $$PROTO_SOURCE_PATH:/app/in \
		-it \
		--rm \
		dockergrpc:$$DOCKERGRPC_GITHUB_IMAGE_TAG \
		make generate-github

generate-google:
	cd ./in && \
		protoc --go_out=../out *.proto && \
		protoc --go-grpc_out=../out *.proto

generate-github:
	cd ./in && \
		protoc --go_out=plugins=grpc:../out *.proto && \
		protoc --doc_out=../out  --doc_opt=html,proto-platformbanking-doc.html *.proto

clear:
	rm -rf ./out/*